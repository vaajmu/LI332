type fmark = Bold | Underline | Italic ;;
type fchar = (char * fmark list) ;;
type fline = fchar list ;;

let set_mark l a = a::l ;;
let unset_mark l a = List.filter ( (<>) a ) l ;;
let present_mark l a = List.mem a l ;;
let change_mark l a =
  if present_mark l a then unset_mark l a
  else set_mark l a ;;


module type INPUT_SOURCE =
sig
  type t
  type param
  exception End
  val new_input: param -> t
  val input_line: t -> string
  val close_input: t -> unit
end

module type INPUT_ANALYZER =
sig
  val analyze: string -> fline
end 

module Input (Source: INPUT_SOURCE) (Analyzer: INPUT_ANALYZER) =
struct
  type t = Source.t
  type param = Source.param 
  let new_input p = (Source.new_input p)
  let input_line c = Analyzer.analyze (Source.input_line c)
  let close_input c = (Source.close_input c)
end

module File_input : INPUT_SOURCE =
struct
  type t = in_channel
  type param = string
  exception End
  let new_input chemin = open_in chemin
  let input_line flux =
    let line = try input_line flux with End_of_file -> raise End in
      line
  let close_input flux =
    close_in flux
end

module Console_input : INPUT_SOURCE =
struct
  let alias_unit : unit = ()
  type param = ()
  let alias_param : param = ()
  type t = ()
  let alias_t : t = ()
  exception End
  let new_input (s : param) : t = alias_t
  let input_line (f : t) : string = try read_line alias_unit with End_of_file -> raise End
  let close_input (f : t) : unit = alias_unit
end

let str2liste chaine = 
  let mot2liste mot =
    let rec aux mot i taille =
      if taille = i then mot.[i] :: []
      else mot.[i] :: (aux mot (i+1) taille)
    in
      aux mot 0 (String.length mot - 1)
  in
    mot2liste chaine;;

module Verbatim_input : INPUT_ANALYZER =
struct
  let analyze chaine = 
    let char2fchar (c : char) : fchar = (c, []) in
    let liste2fline l = List.map (char2fchar) l in
      (liste2fline (str2liste chaine))
end

module Markdown_input : INPUT_ANALYZER =
struct
  let analyze chaine =
    let a = ref [] and l = ref [] in
    let char2fchar (c : char) (a : fmark list ref) (l : fline ref) : (unit) =
      match c with      
	  '*' -> (a := (change_mark !a Bold)); ()
	| '/' -> (a := (change_mark !a Italic)); ()
	| '_' -> (a := (change_mark !a Underline)); ()
	| lettre -> (l := ( (lettre, !a)::!l )); ()
    in
      List.iter (fun (el) -> (char2fchar el a l) ) (str2liste chaine); !l
end

module type OUTPUT_DEST =
sig
  type param 
  type t
  val new_output : param -> t
  val output_line : t -> string -> unit
  val close_output : t -> unit
end
  
module type OUTPUT_RENDERER =
sig
  val render : fline -> string
end

module Output (Dest : OUTPUT_DEST) (Renderer : OUTPUT_RENDERER) =
struct
  type t = Dest.t
  type param = Dest.param
  let new_output p = (Dest.new_output p)
  let output_line p1 p2 = (Dest.output_line p1 (Renderer.render p2))
  let close_output p = (Dest.close_output p)
end

module File_output : OUTPUT_DEST =
struct
  type param = string
  type t = out_channel
  let new_output cheminom = open_out cheminom
  let output_line flux chaine = output_string flux chaine
  let close_output flux = close_out flux
end

module Console_output : OUTPUT_DEST =
struct
  type param = string
  type t = unit
  let new_output rien = ()
  let output_line rien chaine = print_string chaine
  let close_output rien = ()
end

module Verbatim_output : OUTPUT_RENDERER =
struct
  let render ligne = String.concat "" (List.map ( fun((c,l)) -> (String.make 1 c) ) ligne)
end

module Html_output : OUTPUT_RENDERER =
struct
  let render ligne = 
    let fmark2ostring mark =
      match mark with
	  Bold -> "<b>"
	| Underline -> "<u>"
	| Italic -> "<i>"
    in
    let fmark2cstring mark =
      match mark with
	  Bold -> "</b>"
	| Underline -> "</u>"
	| Italic -> "</i>"
    in
    let fmarklist2ostring l =
      String.concat "" (List.map fmark2ostring l)
    in
    let fmarklist2cstring l =
      String.concat "" (List.map fmark2cstring l)
    in
    let fchar2string ((c, l) : fchar) =
      String.concat "" [(fmarklist2ostring l);(String.make 1 c);(fmarklist2cstring (List.rev l))]
    in
      String.concat "" (List.map fchar2string ligne)
end

module type PROCESSOR =
sig
  type input_param
  type output_param
  val process : input_param -> output_param -> unit
end

module Processor (Input : INPUT) (Output : OUTPUT) : (PROCESSOR 
						      with type input_param = Input.param 
						      and type output_param = Output.param) =
struct
  let process ip op =
    let inf = Input.new_input ip and outf = Output.new_output op 
    in
      try
	while true do
	  Output.output_line outf (Input.input_line inf)
	done
      with Input.End -> Input.close_input inf; Output.close_output outf
end
  

;;

