(*

  projet__DE_WARGNY__LOHEAC.ml

  compilation :
  ocamlc projet__DE_WARGNY__LOHEAC.ml -o projet__DE_WARGNY__LOHEAC

*)


(* Import *)
#load "graphics.cma";;
open Graphics
#use "midi.ml"

(* Ex3 Q1 *)
type obj_musical = Note of (int*int*int) 
		   | Silence of (int) 
		   | Sequence of (obj_musical list) 
		   | Parallele of (obj_musical list) ;;

(* Ex3 Q2 *)
let rec duration obj = 
  match obj with
      Parallele list -> List.fold_left max 0 (List.map duration list)
    | Silence(sil) -> sil
    | Note(h, d, v) -> d
    | Sequence list -> List.fold_left (+) 0 (List.map duration list) ;;

let rec copy obj =
  match obj with
      Note (h, d, v) -> Note(h, d, v)
    | Silence (d) -> Silence (d)
    | Sequence (list) -> Sequence (List.map copy list)
    | Parallele list -> Parallele (List.map copy list) ;;

let rec note_count obj = 
  match obj with
      Note (h, d, v) -> 1
    | Silence (sil) -> 0
    | Sequence list -> List.fold_left (+) 0 (List.map note_count list)
    | Parallele list ->  List.fold_left (+) 0 (List.map note_count list) ;;

let rec stretch obj float =
  match obj with
      Note (h, d, v) -> Note(h, int_of_float(float_of_int(d) *. float), v)
    | Silence (sil) -> Silence (int_of_float(float_of_int(sil) *. float))
    | Sequence list -> Sequence (List.map (fun (el) -> stretch el float) list)
    | Parallele list -> Parallele (List.map (fun (el) -> stretch el float) list) ;;

(* Ex4 *)
let rec transpose obj n = 
  match obj with
      Note (h, d, v) -> Note (h+n, d, v)
    | Silence (sil) -> Silence (sil)
    | Sequence list -> Sequence (List.map (fun (el) -> transpose el n) list)
    | Parallele list -> Parallele (List.map (fun (el) -> transpose el n) list) ;; 

let rec retrograde obj =
  match obj with
      Note (h, d, v) -> Note (h, d, v) 
    | Silence (sil) -> Silence (sil)
    | Sequence list -> Sequence (List.rev (List.map retrograde list))
    | Parallele list -> Parallele (List.map retrograde list) ;;

let rec mirror obj n = 
  match obj with
      Note (h, d, v) -> Note ((2*n)-h , d, v)
    | Silence (sil) -> Silence (sil)
    | Sequence list -> Sequence (List.map (fun (el) -> mirror el n) list)
    | Parallele list -> Parallele (List.map (fun (el) -> mirror el n) list) ;;

let repeat obj n =
  let rec aux obj n = 
    match n with
	0 -> []
      | _ -> (obj::(aux obj (n-1)))
  in
    Sequence(aux obj n) ;;

let canon obj n =
  Parallele([obj; Sequence([Silence(n);obj])]) ;;

let rec concat_mo obj1 obj2 =
  Sequence ([obj1; obj2]);;

(* Ex5 *)
let affiche_mo objet =
  let mo = (stretch objet 0.1) in
  let epaisseur = 2 in
  let fw = (duration mo) + 50 in
  let fh = 127 * epaisseur in
  let fo = " " ^ (string_of_int fw) ^ "x" ^ (string_of_int fh) ^ "+100+10" in

  let rec aux obj x =
    match obj with
	Note (h, d, v) -> fill_rect x (h*epaisseur) d epaisseur
      | Silence (sil) -> () 
      | Sequence ([]) -> ()
      | Sequence (hd::tl) -> (aux hd x); (aux (Sequence tl)) (x+(duration hd))
      | Parallele ([]) -> ()
      | Parallele (hd::tl) -> (aux hd x); (aux (Sequence tl)) x
  in

  let rec continue () =
    if (wait_next_event [Key_pressed]).key = 'q' then ()
    else continue ()
  in
    open_graph fo;
    aux mo 25;
    continue ();
    close_graph ()
;;

let obj2midi (obj : obj_musical) : (int * track list) =
  let rec obj2event obj n =
    match obj with
	Note(h, d, v) -> (n, 1, NoteON(h, v))::(n+d, 1, NoteOFF(h, v))::[]
      | Silence(sil) -> []
      | Sequence [] -> []
      | Sequence (hd::tl) -> (obj2event hd n)@(obj2event (Sequence(tl)) (n+(duration hd)))
      | Parallele list -> List.fold_left (@) [] (List.map (fun(obj) -> obj2event obj n) list)
  in
  let cmp e1 e2 =
    match e1, e2 with
	(d1, _, _),(d2, _, _) when d1 < d2 -> -1
      | (d1, _, _),(d2, _, _) when d1 > d2 -> 1
      | _,_ -> 0
  in
  let rec convert liste t0 =
    match liste with
	[] -> []
      | (t1, c, me)::tl -> let d = (t1-t0) in (d, c, me)::(convert tl t1)
  in
    (1000,((convert (List.sort cmp (obj2event obj 0)) 0)) :: []) ;;

let save_as_midi obj =
  write (obj2midi obj) "out.mid";;

(* Ex6 *) 
