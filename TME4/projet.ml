(*

  Compilation :
  ocamlc graphics.cma projet.ml -o prg

*)

(* EXERCICE 3 *)

#load "graphics.cma";;
#load "midi.cmo"

type obj_musical = Note of (int*int*int) | Silence of (int) | Sequence of (obj_musical list) | Parallele of (obj_musical list) ;;

(*Note of hauteur, durée et volume*)

let dolol = Note (60, 1000, 80) ;;
let mi = Note (64, 500, 80) ;;
let re = Note (62, 500, 80) ;;
let sil = Silence (1000) ;;
let sol = Note (67, 1000, 80) ;;

let mibis = Note (52, 2000, 80) ;;
let solbis = Note (55, 1000, 80) ;;

let seq1 = Sequence ([dolol; mi; re; sil; sol]) ;;
let seq2 = Sequence ([mibis; solbis; solbis]) ;;

let exemple = Parallele ([seq1; seq2]) ;;


let rec duration obj = 
  match obj with
      Parallele list -> List.fold_left max 0 (List.map duration list)
    | Silence(sil) -> sil
    | Note(h, d, v) -> d
    | Sequence list -> List.fold_left (+) 0 (List.map duration list) ;;

duration exemple;;


let rec copy obj =
  match obj with
      Note (h, d, v) -> Note(h, d, v)
    | Silence (d) -> Silence (d)
    | Sequence (list) -> Sequence (List.map copy list)
    | Parallele list -> Parallele (List.map copy list);;

let tout = copy exemple;;

tout;;

let rec count obj = 
  match obj with
      Note (h, d, v) -> 1
    | Silence (sil) -> 0
    | Sequence list -> List.fold_left (+) 0 (List.map count list)
    | Parallele list ->  List.fold_left (+) 0 (List.map count list) ;;

count tout ;;

let rec stretch obj float =
  match obj with
      Note (h, d, v) -> Note(h, int_of_float(float_of_int(d) *. float), v)
    | Silence (sil) -> Silence (int_of_float(float_of_int(sil) *. float))
    | Sequence list -> Sequence (List.map (fun (el) -> stretch el float) list)
    | Parallele list -> Parallele (List.map (fun (el) -> stretch el float) list) ;;




(* EXERCICE 4 *)

let rec transpose obj n = 
  match obj with
      Note (h, d, v) -> Note (h+n, d, v)
    | Silence (sil) -> Silence (sil)
    | Sequence list -> Sequence (List.map (fun (el) -> transpose el n) list)
    | Parallele list -> Parallele (List.map (fun (el) -> transpose el n) list) ;; 

transpose tout 12 ;;

let rec retrograde obj =
  match obj with
      Note (h, d, v) -> Note (h, d, v) 
    | Silence (sil) -> Silence (sil)
    | Sequence list -> Sequence (List.rev list)
    | Parallele list -> Parallele (List.rev (List.map retrograde list)) ;;

tout ;;
retrograde tout ;;


let rec miroir obj n = 
  match obj with
      Note (h, d, v) -> Note ((2*n)-h , d, v)
    | Silence (sil) -> Silence (sil)
    | Sequence list -> Sequence (List.map (fun (el) -> miroir el n) list)
    | Parallele list -> Parallele (List.map (fun (el) -> miroir el n) list) ;;

tout ;;
miroir tout 60;; 

(*let rec aux_repete a n =
  match n with 
      0 -> []
    | _ -> a::(aux_repete a (n-1)) ;;

let rec aux_repbis l n =
  match n with
      0 -> []
    | _ -> l@(aux_repbis l (n-1)) ;; 

aux_repbis [1;2;3;4;5] 2 ;;

let rec repeat obj n = 
  match obj with
      Note (h, d, v) -> Sequence (aux_repete (Note (h, d, v)) n)
    | Silence (sil) -> Sequence (aux_repete (Silence (sil)) n)
    | Sequence list -> Sequence (aux_repbis list n) 
    | Parallele list -> Parallele (List.map (fun(el) -> repeat el n) list) ;;
*)


let repeat obj n =
  let rec aux obj n = 
    match n with
	0 -> []
      | _ -> (obj::(aux obj (n-1)))
  in
    Sequence(aux obj n) ;;

tout ;;
repeat tout 2 ;;
repeat seq1 2 ;;

let rec decale obj n =
  match obj with
      Sequence list -> Sequence (Silence(n)::list)
    | Note(h, d, v) -> decale (Sequence ([Note(h,d,v)])) n
    | _ -> obj ;;

let merge p1 p2 =
  match (p1, p2) with
      (Parallele list1, Parallele list2) -> Parallele (list1@list2)
    | _,_ -> p1 ;;

let decale_para obj n =
  match obj with
      Parallele list -> Parallele (List.map (fun(el) -> decale el n) list) 
    | _ -> Parallele([]);;

tout ;;
decale_para tout 100 ;;

seq1 ;;
decale (Note(10,10,10)) 100;;
decale seq1 100 ;;

let rec canon obj n =
  match obj with
      Note (h, d, v) -> Parallele ([Sequence([Note(h,d,v)]); (decale (Note(h,d,v)) n)])
    | Silence (sil) -> obj
    | Sequence list -> Parallele ([obj; (decale obj n)])
    | Parallele list -> merge obj (decale_para obj n ) ;;


seq1 ;;
canon seq1 100;;

tout ;;
canon tout 100 ;;


let rec concat_mo obj1 obj2 =
  Sequence ([obj1; obj2]);;

open Graphics;;

duration tout;;

(*
let note_width d =
  int_of_float ( (float_of_int d) *. 0.2);;
*)
(*
  let epaisseur = 2;;
  let hLigne = epaisseur;;
  let fw = (note_width (duration tout)) + 50;;
  let fh = 127 * hLigne;;
  let fo = " " ^ (string_of_int fw) ^ "x" ^ (string_of_int fh) ^ "+100+10";;
*)

(*
  let affiche_mo obj =
  let rec aux obj x =
  match obj with
  Note (h, d, v) -> fill_rect x (h*hLigne) (note_width d) (hLigne) (*APRES AVANT*)
  | Silence (sil) -> () 
  | Sequence ([]) -> ()
  | Sequence (hd::tl) -> (aux hd x); (aux (Sequence tl)) (x+(note_width (duration hd)))
  | Parallele ([]) -> ()
  | Parallele (hd::tl) -> (aux hd x); (aux (Sequence tl)) x
  in
(* Appel à stretch pour réduire la taille de la partition *)
  open_graph fo;
  aux obj 25;
  continue ();
  close_graph ()
  ;;
*)

let affiche_mo objet =
  let mo = (stretch objet 0.1) in
  let epaisseur = 2 in
  let fw = (duration mo) + 50 in
  let fh = 127 * epaisseur in
  let fo = " " ^ (string_of_int fw) ^ "x" ^ (string_of_int fh) ^ "+100+10" in

  let rec aux obj x =
    match obj with
	Note (h, d, v) -> fill_rect x (h*epaisseur) d epaisseur
      | Silence (sil) -> () 
      | Sequence ([]) -> ()
      | Sequence (hd::tl) -> (aux hd x); (aux (Sequence tl)) (x+(duration hd))
      | Parallele ([]) -> ()
      | Parallele (hd::tl) -> (aux hd x); (aux (Sequence tl)) x
  in

  let rec continue () =
    if (wait_next_event [Key_pressed]).key = 'q' then ()
    else continue ()
  in
  
  open_graph fo;
  aux mo 25;
  continue ();
  close_graph ()
;;

affiche_mo tout;;


(*open_graph " 900x800+100+10";*)
(*Graphics.fill_rect x h d (h+hLigne) *)
(*Graphics.open_graph ""*)
(*List.map (fun(el) -> aux (List.fold_left (+) x (List.map (fun(el) -> ) list ))*)  
(*Note (h, d, v) -> (moveto x (h*hLigne)); (lineto (x+(note_width d)) ((h*hLigne)))*) (*AVEC DES LIGNES*)
(*Note (h, d, v) -> Graphics.fill_rect x h (note_width d) (hLigne)*)  (*AVANT*)




(* MIDI track : 
   (int * int * midievent) list
   (deltaT * channel * event) list
*)

(* MIDI structure : 
   int * track list
   division * (track list)
*)

(*
let ajoute_note el l =
  match !l with
      [] -> failwith "Liste vide"
    | hd::tl -> l := (hd@el) :: tl;;
*)
(*
let om2midi objet =
  let liste = ref [[]] in
  let rec aux obj d=
    match obj with
	Note(h, d, v) -> ajoute_note ((0, 0, NoteON (h, v)) :: (d, 0, NoteOFF (h, v)) :: []) liste
      | Silence (sil) -> aux (Note (0, sil, 0))
      | Sequence (hd::tl) -> (aux hd d); (aux (Sequence tl) (d + (duration hd)))
      | Parallele (list) -> List.map (fun(obj) -> aux obj d) list;;
*)

(*((0, 0, NoteON (0, 0)) :: (sil, 0, NoteOFF (0, 0)) :: []) :: []*)

let obj2midi (obj : obj_musical) : (int * track list) =
  
  let rec obj2event obj n =
    match obj with
	Note(h, d, v) -> (n, 1, NoteON(h, v))::(n+d, 1, NoteOFF(h, v))::[]
      | Silence(sil) -> [] (*obj2event (Note(0, sil, 0)) n*)
      | Sequence [] -> []
      | Sequence (hd::tl) -> (obj2event hd n)@(obj2event (Sequence(tl)) (n+(duration hd)))
      | Parallele list -> List.fold_left (@) [] (List.map (fun(obj) -> obj2event obj n) list)
  in
  let cmp e1 e2 =
    match e1, e2 with
	(d1, _, _),(d2, _, _) when d1 < d2 -> -1
      | (d1, _, _),(d2, _, _) when d1 > d2 -> 1
      | _,_ -> 0
  in
  let rec convert liste t0 =
    match liste with
	[] -> []
      | (t1, c, me)::tl -> let d = (t1-t0) in (d, c, me)::(convert tl t1)
  in
    (1000,((convert (List.sort cmp (obj2event obj 0)) 0)) :: [])
;; 

obj2midi tout;;

let save_as_midi obj =
  write (obj2midi obj) "out.mid";;

obj2midi do60;;
save_as_midi do60;;
save_as_midi tout;;

read "out.mid";;

let toutou =  (repeat tout 3) ;;
affiche_mo toutou ;;
save_as_midi toutou;;

let do60 = Note (60,1500,80);;
let recroche = Note (62,500,80);;
let mibemolcroche = Note (63,500,80);;
let facroche = Note (65,500,80);;
let fadcroche = Note (66,500,80);;
let microche = Note (64,500,80);;
let silence = Silence(2000);;
let solblanche = Note (67,2000,80);;
let labnoirep = Note (68,1250,80);;
let fadc = Note (65,250,80);;
let do60ddc = Note (61,250,80);;
let do60dc = Note (60,250,80);;
let sibecartnoire = Note (59,1000,80);;
let fadblanche = Note (66,2000,80);;
let fablanche = Note (65,2000,80);;
let miblanche = Note (64,2000,80);;
let mibemolblanche = Note (63,2000,80);;
let renoirepoint = Note (62,1500,80);;
let do60dcroche = Note (61,500,80);;
let sibemolcroche = Note (58,500,80);;
let labecartcroche = Note (57,500,80);;
let renoire = Note (62,1000,80);;
let fanoire = Note (65,1000,80);;
let premiervoix = Sequence
[do60;recroche;mibemolcroche;microche;facroche;fadcroche;solblanche;labnoirep;fadc;do60ddc;do60dc;sibecartnoire;
                        silence;solblanche;fadblanche;fablanche;miblanche;mibemolblanche;renoirepoint;do60dcroche;sibemolcroche;
                        labecartcroche;renoire;silence;solblanche;fanoire;miblanche];;
                       
                       
Deuxieme voix:
let quartsoupir = Silence(250);;
let dogravedc = Note (48,250,80);;
let migravedc = Note (51, 250,80);;
let solgravedc = Note (55,250,80);;
let do60blanche = Note (60,2000,80);;
let sibemolnoirep = Note (58,1250,80);;
let mibecgravedc = Note (52, 250,80);;
let regravedc = Note (50, 250,80);;
let fagravedc = Note (53, 250,80);;
let labgravenoirep = Note (56,1500,80);;
let labgravec = Note (56,500,80);;
let solgravec = Note (55,500,80);;
let fagravec = Note (53, 500,80);;
let mibemolnoirep = Note (51,1250,80);;
let regravec = Note (50, 500,80);;
let dogravec = Note (48,500,80);;
let dogravediesen = Note (49,500,80);;
let demisoupir = Silence(500);;
let migravec = Note (51,500,80);;
let sibecgravedc = Note (47,250,80);;
let labecggravedc = Note (45,250,80);;

let deuxiemevoix = Sequence
 [
quartsoupir;dogravedc;migravedc;solgravedc;do60blanche;sibemolcroche;labecartcroche;sibemolnoirep;mibecgravedc;regravedc;mibecgravedc;fagravedc;
                        dogravedc;fagravedc;solgravedc;labgravenoirep;labgravec;solgravec;fagravec;mibemolnoirep;migravedc;fagravedc;migravedc;regravec;dogravec;dogravediesen;
                        demisoupir;regravec;migravec;regravec;dogravedc;sibecgravedc;dogravedc;sibecgravedc;dogravedc;regravedc;dogravedc;sibemolgravedc;labecggravedc;]
*) 

(*
# obj2midi tout;;
- : int * (int * int * midievent) list list =
(1000,
 [[(0, 1, NoteON (60, 80)); (0, 1, NoteON (52, 80));
   (1000, 1, NoteOFF (60, 80)); (0, 1, NoteON (64, 80));
   (500, 1, NoteOFF (64, 80)); (0, 1, NoteON (62, 80));
   (500, 1, NoteOFF (62, 80)); (0, 1, NoteON (0, 0));
   (0, 1, NoteOFF (52, 80)); (0, 1, NoteON (55, 80));
   (1000, 1, NoteOFF (0, 0)); (0, 1, NoteON (67, 80));
   (0, 1, NoteOFF (55, 80)); (0, 1, NoteON (55, 80));
   (1000, 1, NoteOFF (67, 80)); (0, 1, NoteOFF (55, 80))]])
*)
