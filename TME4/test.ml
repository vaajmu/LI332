(*

  Tests sur les fonctions de projet__DE_WARGNY__LOHEAC.ml

*)

#use "projet__DE_WARGNY__LOHEAC.ml"

(*
  Creation de notes
*)

let do601000 = Note (60, 1000, 80)
and  mi64500 = Note (64, 500, 80)
and re62500 = Note (62, 500, 80)
and sil1000 = Silence (1000)
and sol671000 = Note (67, 1000, 80)

and mi522000 = Note (52, 2000, 80)
and sol551000 = Note (55, 1000, 80);;

let seq1 = Sequence ([do601000; mi64500; re62500; sil1000; sol671000]);;
let seq2 = Sequence ([mi522000; sol551000; sol551000]);;

let exemple = Parallele ([seq1; seq2]) ;;



let do60 = Note (60,1500,80);;
let recroche = Note (62,500,80);;
let mibemolcroche = Note (63,500,80);;
let facroche = Note (65,500,80);;
let fadcroche = Note (66,500,80);;
let microche = Note (64,500,80);;
let silence = Silence(2000);;
let solblanche = Note (67,2000,80);;
let labnoirep = Note (68,1250,80);;
let fadc = Note (65,250,80);;
let do60ddc = Note (61,250,80);;
let do60dc = Note (60,250,80);;
let sibecartnoire = Note (59,1000,80);;
let fadblanche = Note (66,2000,80);;
let fablanche = Note (65,2000,80);;
let miblanche = Note (64,2000,80);;
let mibemolblanche = Note (63,2000,80);;
let renoirepoint = Note (62,1500,80);;
let do60dcroche = Note (61,500,80);;
let sibemolcroche = Note (58,500,80);;
let labecartcroche = Note (57,500,80);;
let renoire = Note (62,1000,80);;
let fanoire = Note (65,1000,80);;
let premiervoix = Sequence
[do60;recroche;mibemolcroche;microche;facroche;fadcroche;solblanche;labnoirep;fadc;do60ddc;do60dc;sibecartnoire;
                        silence;solblanche;fadblanche;fablanche;miblanche;mibemolblanche;renoirepoint;do60dcroche;sibemolcroche;
                        labecartcroche;renoire;silence;solblanche;fanoire;miblanche];;
                       
                       
Deuxieme voix:
let quartsoupir = Silence(250);;
let dogravedc = Note (48,250,80);;
let migravedc = Note (51, 250,80);;
let solgravedc = Note (55,250,80);;
let do60blanche = Note (60,2000,80);;
let sibemolnoirep = Note (58,1250,80);;
let mibecgravedc = Note (52, 250,80);;
let regravedc = Note (50, 250,80);;
let fagravedc = Note (53, 250,80);;
let labgravenoirep = Note (56,1500,80);;
let labgravec = Note (56,500,80);;
let solgravec = Note (55,500,80);;
let fagravec = Note (53, 500,80);;
let mibemolnoirep = Note (51,1250,80);;
let regravec = Note (50, 500,80);;
let dogravec = Note (48,500,80);;
let dogravediesen = Note (49,500,80);;
let demisoupir = Silence(500);;
let migravec = Note (51,500,80);;
let sibecgravedc = Note (47,250,80);;
let labecggravedc = Note (45,250,80);;

let deuxiemevoix = Sequence
 [
quartsoupir;dogravedc;migravedc;solgravedc;do60blanche;sibemolcroche;labecartcroche;sibemolnoirep;mibecgravedc;regravedc;mibecgravedc;fagravedc;
                        dogravedc;fagravedc;solgravedc;labgravenoirep;labgravec;solgravec;fagravec;mibemolnoirep;migravedc;fagravedc;migravedc;regravec;dogravec;dogravediesen;
                        demisoupir;regravec;migravec;regravec;dogravedc;sibecgravedc;dogravedc;sibecgravedc;dogravedc;regravedc;dogravedc;sibemolgravedc;labecggravedc;]




duration exemple;;

copy exemple;;

affiche_mo exemple; affiche_mo (copy exemple);;

note_count exemple;;

affiche_mo exemple; affiche_mo (stretch exemple 0.5);;

affiche_mo (transpose exemple 12);;

affiche_mo (retrograde exemple);;

affiche_mo (mirror exemple 60);;

affiche_mo (repeat exemple 3);;

affiche_mo (decale exemple 1000);;

affiche_mo (concat_mo exemple exemple);;

affiche_mo seq1;;

affiche_mo (canon exemple 1000);;
