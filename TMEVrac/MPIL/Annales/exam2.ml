type comparaison = Inf | Egal | Sup

module type TYPE_ORDONNE =
sig
  type t
  val compare: t -> t -> comparaison
end


module Mesurable = struct
 class virtual mesurable = 
   object 
     method virtual distance : unit -> float
   end
end

module type  MESURABLE = sig
    class virtual mesurable :
      object method virtual distance : unit -> float end
end

(***********)

(* Q1 *) 
(* on cherche à implanter un module M qui soit compatible avec la signature TYPE_ORDONNE et dont le type t correspond au type mesurable défini dans le module Mesurable (sans le redéfinir) *)

module M = 
struct
  include Mesurable   (* OU voir a: *)
  type t = mesurable   (* a: type t = Mesurable.mesurable *)
  let compare m1 m2 = 
    let d1 = m1#distance()
    and d2 = m2#distance() in 
      if d1 = d2 then Egal 
      else if d1 < d2 then Inf else Sup
end



(* Q2 : limiter ... *)




module M2 = (M:TYPE_ORDONNE)

(* Q3  : définir le module Cart qui définit une classe coordCart héritant de la classe mesurable du module Mesurable : peut-être en Q1 ?? *)

module Cart =
struct 
  include Mesurable  (* OU voir a:  *) 
class coordCart x y = 
object(self)
  inherit mesurable    (*  a: inherit Mesurable.mesurable *)
  val x = x
  val y = y
  method distance () = sqrt(x*.x +. y*.y) 
end
end

(* Q4 : comparer ce que cela fait *)

let pe1 = new Cart.coordCart 2.0 3.0 ;;
let pe2 = new Cart.coordCart 3.0 4.0 ;;

let r1 = M.compare pe1 pe2
(* let r2 = M2.compare pe1 pe2 *)


(* Q5 *)

(* inutile *)
module type S = 
sig
  type  mesurable = <distance : unit -> float>
end


module F (P : MESURABLE) = 
(
struct 
  type t = P.mesurable 
  let compare m1 m2 = 
    let d1 = m1#distance()
    and d2 = m2#distance() in 
      if d1 = d2 then Egal 
      else if d1 < d2 then Inf else Sup
end : TYPE_ORDONNE with type t = P.mesurable
)


module R = F(Cart)

let r = R.compare pe1 pe2

(* Q6 : coordonnées polaires *)

module Pol =
struct 
  include Mesurable  (* OU voir a:  *) 
class coordPol (a:float) (r:float) = 
object(self)
  inherit mesurable    (*  a: inherit Mesurable.mesurable *)
  val a  = r
  val r = r
  method distance () = r
end
end

let pp1 = new Pol.coordPol 90.0 10.0
let pp2 = new Pol.coordPol 180.0 20.0


(* on crée une liste de Mesurable *)

module R2 = F(Pol)

let r2 = R2.compare pp1 pp2
 

(* Q7 *)
(* on termine sur une liste de points *)

module Polygone  = 
struct 
  include Mesurable 
  class polygone (m1:mesurable) (m2:mesurable) = 
  object 
    val mutable l = [m1;m2]
    method add m = l <- m::l
    method distance () = 
      let ld = List.map (fun x -> x#distance()) l in 
        List.fold_left min 0.0 ld
  end 
end 


let pl1 = new Polygone.polygone pe1 pp1;;
pl1#add pe2;;
let r = pl1#distance();;

let pl2 = new Polygone.polygone pe1 pp1;;
pl2#add pp2;;
let r = pl2#distance();;

module R3 = F(Polygone)

(* let r3 = R3.compare pl1 pl2 *)
 let r4 = R3.compare (pl1:>Mesurable.mesurable)  (pl2:>Mesurable.mesurable)

