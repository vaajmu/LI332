(* exo 1 *)
type var = {nom : string; mutable e : expr}
and oper = Add | Sub | Mul | Div 
and  expr = 
  |  R
  |  C of float 
  |  V of var
  |  B of oper * expr * expr 
  |  F of (float list -> float) * expr list (* Q6 *)
;;


(* Q1 *)
(*  
  x + y
 (x + 1.1) * (x + 2.2)
 y + 1
*)

let var_x = {nom="x"; e = R} ;;
let var_y = {nom="y"; e = R} ;;
let e1 = B (Add, V var_x, V var_y) ;;
let e2 = B ( Mul,  (B (Add, V var_x, C 1.1)), (B (Add, V var_x, C 2.2))) ;;

(* Q2 *)
 (* 
    x <- 3.3
    y <- x + 2.1
 *)

var_x.e <- C 3.3;
var_y.e <- B (Add, V var_x, C 2.1);;

(* Q3 *)
(* eval *)
exception BadExpr;;
exception VNI of string;;

let eval_op op v1 v2 = match op with 
  Add -> v1 +. v2
  | Mul -> v1 *. v2
  | Sub -> v1 -. v2
  | Div -> v1 /. v2 ;;


let rec eval e = match e with 
  | R -> raise BadExpr
  | C c -> c
  | V v -> (match v.e with R -> raise (VNI v.nom) | _ -> eval v.e)
  | B (op,e1,e2) -> eval_op op (eval e1) (eval e2)	      
  | F (f, el) -> f (List.map eval el) (* Q6 *)
;;

(* Q4 *)
(* cycle *)
let rec check_aux e trace =
    List.memq e trace || 
   ( match e with 
     B (op,e1,e2) -> let ntrace = e::trace in 
                    (check_aux e1 ntrace) || (check_aux e2 ntrace) 
       | F(_,el) -> let ntrace = el@trace in
            List.fold_left (fun a e -> a || (check_aux e ntrace)) false el
     | _ -> false 
   );; 
  
let check e = check_aux e [];;

(* Q5 *)
(* neo eval *) 

exception Cycle;;

let neoeval e = 
   if check e then raise Cycle
   else eval e;;

(* Q6 *)
(* voir plus haut *)

(* Q7 *)

let moyenne l = 
  let somme = List.fold_left (+.) 0.0 l in
  let nb_element = float_of_int (List.length l) in 
    somme /. nb_element ;;

let exp = F (moyenne, [C 1.2; C 3.4; C 5.6]);;

(* tableur *)
(* Q8 *)
let conv i j = "";;

let feuille n = 
  let a = Array.make_matrix n n R in 
  for i=0 to n-1 do 
    for j=0 to n-1 do 
       a.(i).(j) <- V {nom=conv i j; e=R}
    done
  done;
  a;;

(* Q9 *)
let parLigne c f = 
  let m = f.(c) in 
  let l = Array.length m in 
  let ne = List.rev(List.tl (List.rev (Array.to_list m))) in 
  m.(l-1) <- F(moyenne,ne)
;;
    



(* exo 2 *)
(* tiré d'Okasaki 
   et du site https://bitbucket.org/mmottl/pure-fun/src/744d85f5220565e95c4f56688e1f2ef7e7095190/chp3.ml?at=default *)



exception Empty
exception Impossible_pattern of string

let impossible_pat x = raise (Impossible_pattern x)

module type ORDERED = sig
  type t
  val leq : t -> t -> bool
  val toS : t -> string
end


module type HEAP = sig
  module Elem : ORDERED

  type heap

  val empty : heap
  val is_empty : heap -> bool

  val insert : Elem.t -> heap -> heap
  val merge : heap -> heap -> heap

  val find_min : heap -> Elem.t  (* raises Empty if heap is empty *)
  val delete_min : heap -> heap  (* raises Empty if heap is empty *)

  val to_list : heap -> (int * Elem.t) list
  val print : string -> heap -> unit
end


(* Q1 *)

module OrderedString = struct
  type t = string
  let leq (s1:t)  (s2:t)  = s1 <= s2
  let toS (s1:t)   = (s1:string)
end;;

module LeftistHeap (Element : ORDERED) : (HEAP with module Elem = Element) =
struct
  module Elem = Element

  type heap = E | T of int * Elem.t * heap * heap

  (* Q2 val rank : heap -> int = <fun> *)
  let rank = function E -> 0 | T (r,_,_,_) -> r 


(* Q2 : val makeT : Elem.t -> heap -> heap -> heap = <fun> *)
  let makeT x a b =
    if rank a >= rank b then T (rank b + 1, x, a, b)
    else T (rank a + 1, x, b, a)


(* Q3 : *)
  let empty = E
  let is_empty h = h = E

(* Q4 *)
  let rec merge h1 h2 = match h1, h2 with
    | _, E -> h1
    | E, _ -> h2
    | T (_, x, a1, b1), T (_, y, a2, b2) ->
        if Elem.leq x y then makeT x a1 (merge b1 h2)
        else makeT y a2 (merge h1 b2)

(* Q5 *)
  let insert x h = merge (T (1, x, E, E)) h

(* Q6 *)
  let find_min = function E -> raise Empty | T (_, x, _, _) -> x
  let delete_min = function E -> raise Empty | T (_, _, a, b) -> merge a b

(* Q9 *)
let rec to_list h = match h with 
    E -> []
  | T (r,x,fg,fd) -> 
    let tofg = to_list fg 
    and tofd = to_list fd in 
        ((r,x)) :: tofg @ tofd;;

let rec print  s h = match h with 
   E -> print_string "E\n"
  | T (r,x,fg,fd) -> Printf.printf "(%d,%s) %s\n" r (Element.toS x) s;
                     print (s^".g."^(Element.toS x)) fg; 
                     print (s^".d."^(Element.toS x)) fd;; 
end

(* Q7 *)
module M = LeftistHeap(OrderedString);;

(* Q8 *)
let h1 = M.empty;;
let h2 = M.insert "marquise" h1;;
let h3 = M.insert "vos yeux" h2;;
let h4 = M.insert "d'amour" h3;;
let h5 = M.insert "me font" h4;;
let h6 = M.insert "mourrir" h5;;

let i1 = M.empty;;
let i2 = M.insert "e" i1;;
let i3 = M.insert "s" i2;;
let i4 = M.insert "i" i3;;
let i5 = M.insert "u" i4;;
let i6 = M.insert "q" i5;;
let i7 = M.insert "r" i6;;
let i8 = M.insert "a" i7;;
let i9 = M.insert "m" i8;;


 
(* Q9 *)

(*
let rec mto_list h = match h with 
    M.E -> [(-1,"v"),(-2,"g"),(-3,"d")]
  | M.T (r,x,fg,fd) -> 
    let tofg = mto_list fg 
    and tofd = mto_list fd in 
      let g1 = (match List.hd tofg with ((r,x),_,_) -> (r,x))
      and d1 = (match List.hd tofd with ((r,x),_,_) -> (r,x))  in
        ((r,x),g1,d1) :: tofg @ tofd;;

*)

let r = M.to_list h6;;

List.iter (fun (x,y) -> Printf.printf "(%i,%s)" x y ) r;;

M.print "" h6;;

let ri = M.to_list i9;;
List.iter (fun (x,y) -> Printf.printf "(%i,%s)" x y ) ri;;
