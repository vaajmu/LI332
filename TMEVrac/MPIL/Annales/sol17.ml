

(* Q1 énoncé *)

class virtual ['a] ens (r :'a) = 
object(this) 
  val racine = r
  val mutable courant = r
  method getCourant = courant
  method intervalle  b = let l = ref [] in
    for i=0 to (b-1) do l := courant :: !l; this#suivant() done;
    List.rev !l
  method reset () = courant <- racine
  method virtual suivant : unit -> unit
end;;

(* 1.1.1 *)

(*
class virtual ['a] ens :
  'a ->
  object
    val mutable courant : 'a
    val racine : 'a
    method getCourant : 'a
    method intervalle : int -> 'a list
    method reset : unit -> unit
    method virtual suivant : unit -> unit
  end

*)

(* 1.1.2 *)

class entierPair r = 
object 
  inherit [int] ens r
  method suivant () = courant <- courant + 2
end;;

(* pour tester *)
let aff it = 
  List.iter (fun x -> print_int x; print_string " ") it;
  print_newline();;

(* 1.1.3 *)

let ep = new entierPair 0;;
aff(ep#intervalle  5);;
aff(ep#intervalle  2);;

(* ajouter un reset entre les 2 appels d'intervalle *)

(* 1.1.4 *)

class entierFib r = 
object 
  inherit [int] ens r 
  val mutable c2 = r
  method suivant () = let tmp = c2 in c2 <- courant; courant <- tmp+courant 
end ;;

let ef = new entierFib 1;;

aff(ef#intervalle  5);;
aff(ef#intervalle  2);;


(* utilitaire *)
let rec first_n n l = match n,l with
  0,_ -> [] 
| _,[] -> failwith "first_n"
|  _,a::q -> a :: first_n (n-1) q ;;

(* Q 1.2 1 *)
 
class virtual ['a] ensMemo r = 
object (self)
  inherit ['a] ens r as super
  val mutable index = 0
  val mutable inter = ( [] : 'a list)
  method reset () = courant <- racine; index <- 0; inter <- []

  method intervalle b = 
    if index >= b then first_n b (List.rev inter)
    else (for i=index to b-1 do self#suivant() done; List.rev inter)
end;;


(* 1.2.2 *)

class entierPairMemo r = 
object 
  inherit [int] ensMemo r
  method suivant () = courant <- courant +2; inter <- courant :: inter; index <- index+1
end

let ep = new entierPairMemo 0;;
aff(ep#intervalle  5);;
aff(ep#intervalle  2);;

(* 1.3.1 *)



class virtual ['a] ensWeak r size = 
object (self)
  inherit ['a] ens r as super
  val mutable index = 0
  val mutable inter = ( Weak.create size : 'a Weak.t)
  method reset () = courant <- racine; index <- 0

  method intervalle  b =   
    let l = ref [] in 
    for i=0 to (b-1) do let x = Weak.get inter i in 
       ( match x with Some v -> courant <- v; l:= v :: !l | None -> self#suivant(); l:= courant :: !l)
    done ; List.rev !l

end;;

(* 1.3.2 *)

class entierFibWeak r size = 
object(self)
  inherit ['a] ensWeak r size as super
  val mutable c2 = r
  method suivant () = let tmp = c2 in 
    c2 <- courant; 
    courant <- tmp+courant;
    Weak.set inter index (Some courant);
    index <- index+1
end ;;

(* 1.3.3 *)

(* le nouvel intervalle doit être recalculé *)

let nfw = new entierFibWeak 1 10;;
aff(nfw#intervalle 4);;
Gc.compact();
aff(nfw#intervalle  2);;


(******************************** *)

(* 2.1.1 *)

(* AE AS BO @S *)

type carte = {s : int; c : char array array} ;;
type robot = char ;;
type direction = N | S | O | E ;;

let vide = '.';;
exception Unknown_map of string;;
exception Bad_format of string *int * string;;
exception No_arg;;
exception Unknown_robot of char;;
exception Duplicate_robot of char;;


let lire_carte nom_carte =
  let taille = ref 0 in  
  let tab_carte = ref [||] in 
  (
  try 
    let ic = open_in nom_carte in
    try 
    let k = ref 0 in
    let t = input_line ic in 
    taille := int_of_string t;
    tab_carte := Array.create_matrix !taille !taille vide ; 
    for i=0 to !taille -1 do 
      let s = input_line ic in 
      let l = String.length s in 
        if l < !taille then raise (Bad_format (nom_carte,l,s))
        else 
          for j=0 to l-1 do !tab_carte.(!k).(j) <- s.[j] done;
        incr k
    done;
    close_in ic
    with 
      | End_of_file -> close_in ic
  with 
    | Sys_error s -> raise (Unknown_map nom_carte) ); 
  { s = !taille; c = !tab_carte}
;;  


let robot_valide c = 
   (('A' <= c) && (c <= 'J')) || (c = '@');;

let liste_robots carte =
  let taille = carte.s   
  and tab_carte = carte.c in 
  let l = ref [] in 
    for i=0 to taille-1 do 
      for j=0 to taille-1 do 
        if tab_carte.(i).(j) <> vide then 
          begin
            let r = tab_carte.(i).(j) in 
              if robot_valide r then 
                if List.mem_assoc r !l then raise (Duplicate_robot r)
                else l := (r,(i,j)) :: !l
              else raise (Unknown_robot r)
          end
      done
    done;
    !l;;

let position r carte = List.assoc r (liste_robots carte);;
  

(* ou *)

exception Found of int * int ;;

let trouve_robot c carte =
  let taille = carte.s and tab_carte = carte.c in  
try 
  for i=0 to taille -1 do 
    for j=0 to taille -1 do 
      if tab_carte.(i).(j) = c then raise (Found (i,j))
    done
  done;
  raise Not_found
with 
  Found(i,j) -> (i,j)
  | Not_found -> raise (Unknown_robot c)
;;


exception Unknown_direction of direction;;

let trouve_dir c = match c with 
  | N -> -1,0
  | S -> 1,0
  | O -> 0,-1
  | E -> 0,1
  |  _ -> raise (Unknown_direction c);;
 
let rec trouve_obstacle x y dx dy carte =
  let taille = carte.s 
  and tab_carte = carte.c in 
  let nx = x+ dx
  and ny = y +dy in 
    if (nx >=0) && (nx < taille) && (ny >= 0) && (ny < taille) then 
      if tab_carte.(nx).(ny) != vide then true,x,y 
      else  trouve_obstacle nx ny  dx dy carte
    else false,-1,-1;;


(* ... *)
