
(* fonctionnel et impératif *)
(* Ecrire un résolveur de Sudoku *)

type case = Fixe of int 
          | Lpos of int list

type grille = case array array

(* type coup = {x : int; y : int ; v : int} *)

exception Coup_illegal of ( int * int) * int
exception Bloque of grille
exception Fini of grille 


(* Q1 *)
(* la 1ère ligne *)

let c3 = [1; 2; 4] ;;
let c4 = [2; 6; 8] ;;
let c6 = [2;4;6;8] ;;
let c7 = [1;4;8;9] ;;
let c8 = [1;2;4;9] ;;
let c9 = [2;4;8] ;;


let l1 = [| 
    Fixe 5; 
    Fixe 3; 
    Lpos c3; 
    Lpos c4; 
    Fixe 7; 
    Lpos c6; 
    Lpos c7; 
    Lpos c8; 
    Lpos c9 |];;


(* Q2 : coup légal *)

let legal g (x,y) v = 
  match g.(x).(y) with 
    Lpos l ->  List.mem v l
    | _ -> false
;;

(* Q3 : ajout d'un coup (légal) *) 

(* si besoin *)
let rec remove x l = 
  match l with 
    [] -> []
    |  a::sl -> if a = x then remove x sl else x::(remove x sl)
;;


(* fonctions fournies *)
let ligne (x,y)  = 
  let r = ref [] in for i=0 to 8 do r := (x,i) :: !r done; !r

let colonne (x,y) = 
  let r = ref [] in for i=0 to 8 do r := (i,y) :: !r done; !r

let region (x,y) = 
  let r = ref [] in 
  for i = x /3 to x/3 + 2 do 
    for j = y /3 to y/3 + 2 do 
      r:= (i,j) :: !r
    done
  done;
  !r

(* enlève une valeur d'une liste de valeur *)


let coup_legal g (x,y) v = 
  let c = g.(x).(y) in 
   match c with 
     Fixe _ -> () 
   | Lpos l -> 
       g.(x).(y) <- Fixe v;
       List.iter (function (x,y) -> 
         let c = g.(x).(y) in 
           match c with  
             Fixe _ -> () 
           | Lpos l -> g.(x).(y) <- Lpos (remove v l)) 
            ((ligne (x,y)) @ (colonne (x,y)) @ (region (x,y)))


(* Q4 : verifie *)

exception Break

let verifie  g f = 
  let r = ref true in 
  let lx = Array.length g in 
  let ly = Array.length g.(0) in
  try   
    for i = 0 to lx - 1  do 
      for j = 0 to ly - 1  do 
        if not (f g.(i).(j)) then (r:= false; raise Break)
      done
    done;
    !r
  with Break -> !r

(* Q5 : fin et bloque *) 

let fin g = 
  verifie g (function Fixe _ -> true | Lpos _ -> false)

let bloque g = 
  verifie g (function Fixe _ -> true | Lpos [] -> false | Lpos _ -> true)
 
(* Q6 : faire une copie du jeu *) 

let copie g =
  let lx = Array.length g in 
  let ly = Array.length g.(0) in
  let r = Array.make_matrix lx ly (Lpos []) in  
    for i = 0 to lx - 1  do 
      for j = 0 to ly - 1  do 
        r.(i).(j) <- g.(i).(j) 
      done 
    done;
    r
 
    
(* Q7 : calcul des coups triés dans l'ordre où il y a le moins de possibilités 
        déclenchement d'une exception si tout est joué : 
            ok 
            bad 
 *)


let cases_jouables  g = 
  let res = ref [] in 
  let lx = Array.length g in 
  let ly = Array.length g.(0) in
   for i=0 to lx-1 do 
     for j=0 to ly-1 do 
       let l = g.(i).(j) in
       match l with 
         Fixe _ -> ()
	 | Lpos [] -> () 
         | Lpos lp ->  res := ((i,j),lp,List.length lp)  :: !res
     done
    done;
    ! res

let coups g = 
  let l = cases_jouables  g in 
(* Question 10 *)
  let lt = 
    List.sort (function ((x,y), lp, llp) -> function ((x2,y2), lp2, llp2) -> if lp < lp2 then -1 else if lp = lp2 then 0 else 1)
            l
(* *)
  in List.flatten (
    List.map (function ((x,y),lp,llp) -> 
                (List.map (fun z -> ((x,y),z)) lp)) lt
    )

(* Q8 joue_un_coup *)
let joue_un_coup g (x,y) v = 
  if not (legal g (x,y) v)  then raise (Coup_illegal ((x,y), v))
  else 
    coup_legal g (x,y) v; 
    if fin g then raise (Fini g)
    else if bloque g then raise (Bloque g)
    else ()
   
  

(* Q9  : Essayer toutes les possibilités de coups - parcours en profondeur
         1 solution 
    *)

let rec joue  g = 
 try 
    let l =  coups g in 
     List.iter (function ((x,y),v) -> let g = copie g in coup_legal g (x,y) v; joue g) l 
 with 
   Bloque g -> ()
   | Fini g -> raise (Fini g)

(* let joue g = 
 try 
    joue_aux g;
    g 
 with
   Fini g -> g
   | Bloque g -> raise PasDe 
*)

(* Q10 : voir question 7*)

