type noeud = Lettre of (char * bool * arbre_lex)
and arbre_lex = noeud list;;

(*let existe (mot:string) (dico:arbre_lex) : (bool) = 
  let rec aux (mot : string) (i : int) (dico : arbre_lex) (d : arbre_lex) : (bool) =
    match dico with
      |	[] -> false
      | Lettre(c, b, a)::tl when c = mot.[i] && (i+1) = (String.length mot) -> b
      | Lettre(c, b, a)::tl when c = mot.[i] ->  aux mot (i+1) tl d
      | hd::tl -> aux mot i tl d
  in
  aux mot 0 dico dico;;*)

let existe mot dico =
  let rec aux mot dico i = 
      match dico with
	[] -> false
      |Lettre(c, b, a)::tl when (c = mot.[i]) -> if ((i+1)=(String.length mot)) 
	then b 
	else 
	  aux mot a (i+1)
      |Lettre(c, b, a)::tl -> aux mot tl i
  in
  aux mot dico 0;;
	  



let mot2liste mot =
  let rec aux mot i taille =
    if taille = i then mot.[i] :: []
    else mot.[i] :: (aux mot (i+1) taille)
  in
  aux mot 0 (String.length mot - 1);;

mot2liste "Bonjour";;

let ajoute mot dico =
  let rec aux mot dico =
    match mot, dico with
	[], _ -> failwith "AjoutMotVide"
      | l::[], [] -> Lettre(l, true, []) :: []
      | l::tl, [] -> Lettre(l, false, (aux tl []) ) :: []
      | l::[], Lettre(c, b, a)::d when l=c && b=true -> failwith "Deja_defini"
      | l::[], Lettre(c, b, a)::d when l=c -> Lettre(l, true, a) :: d
      | l::tl, Lettre(c, b, a)::d when l=c -> Lettre(l, b, (aux tl a)) :: d
      | l::tl, Lettre(c, b, a)::d -> Lettre(c, b, a) :: (aux mot d)
  in
  aux (mot2liste mot) dico;;

let dico = [] ;;
let dico = ajoute "Bonjour" dico;;
let dico = ajoute "Bonne" dico;;
let dico = ajoute "Bonjour" dico;;

existe "Boinne" dico;;

let rec construit l =
 match l with
    [] -> []
  |h::tl -> ajoute h (construit tl);;

construit ["antoine";"est";"un";"gros";"debile";"antoinette"] ;;
(*
let rec aux char l =
  match l with
      [] -> (String.make 1 char)
    | h::tl -> aux ((String.make 1 char)^h) tl);;
*)

(*
let rec liste_de_dict dico =
  match dico with
    [] -> []
    | Lettre(c ,b, [])::tl -> c
    | Lettre(c ,b, a)::tl -> c ^ (liste_de_dict a)

*)

let cons x l =
  x::l;;

let rec liste_de_dict dico =
  match dico with
    [] -> []
  | Lettre(c, b ,a)::tl -> (List.map cons (liste_de_dict tl))

;;
