open Graphics;;

(* TYPES *)

type cell = Vide | Blanc | Noir;;

type board = cell array array ;;

let make_board () = 
  let plateau = Array.make_matrix 10 10 Vide in (
    plateau.(5).(5) <- Noir ;
    plateau.(4).(4) <- Noir ;
    plateau.(5).(4) <- Blanc ;
    plateau.(4).(5) <- Blanc ) ;
  plateau ;;

let cell_size = 40 ;;

(* AFFICHAGE GRAPHIQUE *)

let draw_grid plateau =
  set_color black ;
  for i = 0 to  10 do
    moveto (i*cell_size) 0 ;
    lineto (i*cell_size) (cell_size*10) ;
  done;
  for i = 0 to 10 do
    moveto 0 (i*cell_size) ; 
    lineto (cell_size*10) (i*cell_size);
  done;
;;

let fill_cell_depart board x y =
   set_color green;
   fill_rect (x*cell_size) (y*cell_size) cell_size cell_size;
   match board.(x).(y) with
     |Vide -> ();
     |Blanc -> (set_color white ;
		fill_circle  (x*cell_size+ cell_size/2)
				(y*cell_size+ cell_size/2)
				(cell_size/2 -1) ) ;
       |Noir -> (set_color black ;
		 fill_circle (x*cell_size+ cell_size/2) 
		   (y*cell_size+ cell_size/2)
		   (cell_size/2 -1) );
;;

let display_board plateau = 
  for i = 0 to ((Array.length plateau) -1) do
    for j = 0 to ((Array.length plateau.(0)) -1) do
      fill_cell_depart plateau i j
    done;
  done;
  draw_grid plateau;
  let e = wait_next_event [Key_pressed] in
  if e.keypressed then close_graph();
;;

let display_message s = 
  set_color(white);
  fill_rect 0 (cell_size*10+1) (cell_size*10) (cell_size);
  moveto 0 (cell_size*10+1);
  set_color(blue);
  set_font("-*-helvetica-*-*-*-*-24-*-*-*-*-*-*-*");
  draw_string s 
;;

let is_finished plateau =
  try(
    for y = 0 to Array.length plateau -1 do
      for x = 0 to Array.length plateau.(0) -1 do
	match plateau.(y).(x) with 
	  |Vide -> raise Exit
	  |Noir|Blanc -> ()  
      done ;
    done;
    true
  )
  with 
    |Exit -> false
;;

(* BOUCLE DE JEU *)
(*
let playable_cell board cell (x,y) = 
  try(
    for dy = -1 to 1 do
      for dx = -1 to 1 do
	if not(x+dx < 0 || x+dx > 9 || y+dy < 0 || y+dy > 9) && board.(y).(x) = V then 
	  match board.(y+dy).(x+dx) with 
	    |V -> ()
	    |N -> if cell <> N then 
		 raise Exit
	    |B -> if cell <> B then 
		 raise Exit
      done ;
    done;
    false
  )
  with 
    |Exit -> true
;;

let exists_playable_cell plateau cell  = 
  try (
    for y = 0 to (Array.length plateau -1) do
      for x = 0 to Array.length plateau.(0)-1 do
	match playable_cell plateau cell (x,y) with
	  |true -> raise Exit
	  |false -> ()
      done;
    done;
    false
  ) 
  with |Exit -> true
;; 
*)

(* MAIN *)
let main () =
  let plateau = ref (make_board()) in 
      print_string "affichage" ; print_newline() ;
     open_graph ( Printf.sprintf " %dx%d" 
		     (cell_size * (Array.length !plateau.(0) ) ) 
		     (cell_size * ((Array.length !plateau) + 1) ) ) ; 
      display_board !plateau ;
      display_message "black" 
	
;;

main () ;;
