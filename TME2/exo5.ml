open String ;;

let cherche_car str car =
  if (contains str car) then (index str car) else -1
;;


cherche_car "ozkignorzinfoezki,frpa" 'r';;


let rec espace str =
  if (contains str ',' && contains str '.') 
  then if (index str ',') < index str '.' 
    then (sub str 0 (index str ',')) ^ espace ((sub str (index str ',') ((length str) - (index str ','))))
    else (sub str 0 (index str '.')) ^ espace ((sub str (index str '.') ((length str) - (index str '.'))))
  else
    str
;;


espace "Bonjour je m'appelle Amaury.Ce texte,lol,metunespace.apres.les,points,etles.virgules" ;;
