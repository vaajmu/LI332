let rec calcul_prefixe l =
  match l with
      [] -> 0
    |h::[] -> 1
    |h1::h2::t -> if h1=h2 then 1 + calcul_prefixe (h2::t) else 1 
;;

calcul_prefixe [1;1;1;2;1];;
calcul_prefixe [1;2;1];;
calcul_prefixe [];;
calcul_prefixe [];;

open List;;

let rec genere_list l =
  
  let rec aux l n =
    match l with
	[] -> []
      | h::t -> if n=1 then l else aux t (n-1)
  in
      match l with
      [] -> []
    |h::t -> (calcul_prefixe l)::h::(genere_list (aux t (calcul_prefixe l))) 
;;

genere_list [1];;
genere_list [1;1];;
genere_list [1;1;1;2;1];;
genere_list [1;2;1];;
genere_list [];;
genere_list [1;1;1;2;2;1;1];;

let rec genere_une_liste k =
  match k with
      0-> []
    |1 -> [1]
    |_ -> genere_list (genere_une_liste (k-1))
;;

(*
let genere k = 
  match k with 
      1 -> [[1]]
    |_ -> [[genere (k-1)]] @ [genere_une_liste k]
;;
*)

let rec genere k = 
  match k with 
      1 -> [[1]]
    | x -> (genere (x-1)) @ [(genere_une_liste x)] 
;;



genere 1 ;;
genere 6 ;;
genere 12 ;;
genere 2 ;;
genere 4 ;;


