let rec filtre a b list = 
  match list with
      [] -> []
    | h::t -> if(h<a || h>b) then filtre a b t else h::(filtre a b t) 
;;

let filtre2 a b list =
  List.filter (fun(el)-> (el>=a) && (el<=b) ) list;;

let l1 = [1;2;3;4;5;6;7];;
let l2 = [7;2;5;3;6;4;2];;

filtre 4 7 l1;;
filtre 4 7 l2;;
filtre2 4 7 l1;;
filtre2 4 7 l2;;

let filtre3 ?a ?b list =
  match a, b with
      Some min , Some max -> (filtre min max list)
    | Some min , None -> (List.filter (fun(el)-> (el>=min) ) list)
    | None, Some max -> (List.filter (fun(el)-> (el<=max) ) list)
    | None, None -> list
;;

filtre3 ~a:4 l1;;
filtre3 ~b:4 l1;;
filtre3 ~a:4 ~b:6 l1;;
filtre3 l1;;

let filtre4 variant list=
  match variant with
      `Passe_bande (a, b) -> filtre3 ~a:a ~b:b list
    | `Passe_bas (b) -> filtre3 ~b:b list
    | `Passe_haut (a) -> filtre3 ~a:a list
;;

class virtual ['a] with_timeline =
object
  method virtual on_time :int -> 'a    

end

class ['a] constant param =
object
  inherit ['a] with_timeline
  method on_time a =
    param
end

let boucle couleur titre forme =
  let cpt = ref 0 
  in
    while true do
      cpt := !cpt + 1;
      couleur#on_time
	titre#on_time
	forme#on_time
      
    done
      
