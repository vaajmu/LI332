#include <stdio.h>
#include <caml/mlvalues.h>
#include <caml/callback.h>

value test_afficher (value v) {
  printf("Hello world\n");
  return Val_unit;
}

value generic_print (value v) {
  CAML_Param1(v);
  if (!is_block(v))
    printf("%d\n", Int_val(v));
  else {
    print
