let rec f tab n =
  if n = Array.length(tab) then "\n"
  else tab.(n) ^ " " ^ f tab (n+1) ;;

(*
let echo Sys.argv =
*)

print_string (f Sys.argv 1) ;;

