let rec g tab n =
  if n = 0 then 0.
  else float_of_string tab.(n) +. g tab (n-1);;

let print = print_float (g (Sys.argv) ((Array.length Sys.argv) - 1)); print_string "\n" ;;
