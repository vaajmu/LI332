(*

  type prédéfini

  type 'a option = None | Some 'a 

*)

#load "graphics.cma";;
open Graphics

class virtual shape ((px:int), (py:int)) =
object
  val x = px
  val y = py
  method virtual is_pixel_in : (int*int) -> (int * int * int) option
end 

class disque point r =
object
  inherit shape point
  method is_pixel_in (px, py) = 
    (*if (sqrt ( ((float_of_int px) -. x)**2. +. ((float_of_int py) -. y)**2. ) ) < r then Some (0,0,0)*)
    if (sqrt ( (float_of_int(px - x))**2. +. (float_of_int(py - y))**2. ) ) < (float_of_int r) then Some (0,0,0)
    else None
end    

class rectangle point l h =
object
  inherit shape point
  method is_pixel_in (px, py) =
    if x <= px && x+l >= px && y <= py && y+h >= py then Some (0,0,0)
    else None
end

  
  


let main () =
  let formes = [ new disque (6,6) 3;
		 new rectangle (8,8) 10 3;
		 new rectangle (25,25) 10 3]
  in
  let width = 600 in
  let height = 400 in
  let fo = " " ^ (string_of_int width) ^ "x" ^ (string_of_int height) in
  let rec continue () =
    if (wait_next_event [Key_pressed]).key = 'q' then ()
    else continue ()
  in
  let affiche_liste l = 
    for i=0 to width do
      for j=0 to height do
	
      done
    done
  in
    open_graph fo;
    affiche_liste formes;
    continue ();
    close_graph ()
;;
